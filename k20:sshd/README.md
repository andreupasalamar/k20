# K20 SSH Server

@edt ASIX M11-SAD Curs 2020 - 2021

## Autenticació

* *isx20612296/k20:sshd* Servidor ssh amb pam amb autenticació de kerberos i IP ldap. El host es configura
anb authconfig.
`docker run --rm --name sshd.edt.org -h sshd.edt.org --net 2hisix -it andreupasalamar/k20:sshd`
