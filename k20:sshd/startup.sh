#!/bin/bash

#Instalació
/opt/docker/install.sh && echo "OK install"

# Encenem el servei nslcd, nscd i sshd
/sbin/nslcd && echo "OK nslcd"
/sbin/nscd && echo "OK nscd"
/usr/sbin/sshd -D
