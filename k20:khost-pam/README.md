# K20 Kerberos host client

@edt ASIX M11-SAD Curs 2020 - 2021

## Autenticació

* *isx20612296/k20:khost-pam* Host client de kerberos amb eines basiques de Kerberos. Inclou pam, usuaris locals i usuaris kerberos

`docker run --rm --name khost.edt.org -h khost.edt.org --net 2hisix -it andreupasalamar/k20:khost-pam`
