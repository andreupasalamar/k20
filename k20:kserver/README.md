# K20 Kerberos server

@edt ASIX M11-SAD Curs 2020 - 2021

## Autenticació

* *isx20612296/k20:kserver* Servidor Kerberos detach. Crea principals basics.

* Per posar el server a una AMI
	- Assegurar que tenim la última versió del servidor
	- Assegurar que el Security Group permet les connexions entrants als porrts 464 749 i 88
	- Assegurar de mapejar els ports a l'hora d'iniciar el contenidor
	- Especificar al client on trobar el servidor. Fitxer /etc/hosts


`docker run --rm --name kserver.edt.org -h kserver.edt.org --net 2hisix -d andreupasalamar/k20:kserver`
