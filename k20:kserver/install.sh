#!/bin/bash
# Kserver
# isx20612296@edt.org ASIX-M11 2020-2021

# Copia
cp /opt/docker/krb5.conf /etc/krb5.conf
cp /opt/docker/kdc.conf /var/kerberos/krb5kdc/kdc.conf
cp /opt/docker/kadm5.acl /var/kerberos/krb5kdc/kadm5.acl

# Creació Base de dades
kdb5_util create -s -P masterkey

# Usuaris que s'utilitzaran amb LDAP de IP

for user in anna marta pere pau jordi user{01..10} ; do
  kadmin.local -q "addprinc -pw k$user $user"
done

# Usuaris que s'utilitzaran amb /etc/passwd de IP

for user in kuser{01..06}; do
  kadmin.local -q "addprinc -pw $user $user"
done

kadmin.local -q "addprinc -pw kmarta marta/admin"
kadmin.local -q "addprinc -pw kpere pere/admin"
kadmin.local -q "addprinc -pw kpau pau/admin"
kadmin.local -q "addprinc -pw ksuper super"
kadmin.local -q "addprinc -pw kadmin admin"

kadmin.local -q "addprinc -randkey host/sshd.edt.org"
