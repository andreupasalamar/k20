# k20

Kerberos 2020 EDT - isx20612296

* **andreupasalamar/k20:kserver**: Servidor de LDAP

* **andreupasalamar/k20:khost**: Host client de Kerberos

* **andreupasalamar/k20:khost-pam**: Host client de Kerberos.
	Autentica usuaris contra Kerberos amb ajuda de PAM

* **andreupasalamar/k20:khost-pam-ldap**: Host client de Kerberos i LDAP.
	Autentica usuaris contra els dos serveis.
	Fet amb fedora:27 per poder usar auth-config
