#!/bin/bash
# Khost-pam
# isx20612296@edt.org ASIX-M11 2020-2021

bash /opt/docker/auth.sh

# Copia
cp /opt/docker/krb5.conf /etc/krb5.conf
#cp /opt/docker/ldap.conf /etc/openldap/ldap.conf
#cp /opt/docker/nsswitch.conf /etc/nsswitch.conf
#cp /opt/docker/nslcd.conf /etc/nslcd.conf
cp /opt/docker/ssh_config /etc/ssh/ssh_config

# Crear usuaris local01..03 [IP + AP]
for usuari in local{01..03} ; do
  useradd $usuari
  echo -e "$usuari\n$usuari\n" | passwd --stdin $usuari
done

# Crear usuaris kuser01..kuser03
for usuari in kuser{01..06} ; do
  useradd $usuari
done

# Copia system-auth
#cp /opt/docker/system-auth /etc/pam.d/system-auth

