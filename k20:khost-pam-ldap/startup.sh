#!/bin/bash

#Instalació
/opt/docker/install.sh && echo "OK install"

# Encenem el servei nslcd i nscd
/sbin/nslcd && echo "OK nslcd"
/sbin/nscd && echo "OK nscd"

# Per asegurar de que es queda interactiu
/bin/bash
